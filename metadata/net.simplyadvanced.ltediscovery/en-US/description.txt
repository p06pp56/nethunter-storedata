LTE Discovery is a powerful signal discovery and analysis tool with many advanced features and customizations available.

Key features:
- Identifies LTE band for Verizon, AT&T, Sprint, T-Mobile, and several other countries (support for more carriers added with the crowdsource feature and user aid)
- Identifies EARFCN and Band for any country/provider when using a Qualcomm processor and ROOTED
- Live band identifier and signal data in the notification bar
- Refresh cell radio (reset data connection) to connect to best signal available* (Note: Google added restrictions in Lollipop and above, so it may require root or be unavailable)
- Automatic refreshing of mobile radio for different conditions
- Alert notification for LTE bands and GCIs (connected and disconnected)
- Save LTE logs
- Automated visual logger (Sprint + Pro only)
- EARFCN band calculator (range and precise UL and DL frequencies) (Pro)
- Wide range of settings to customize your experience (Pro)
- Advanced LTE, 4G, 3G, GSM, CDMA data (GCI, PCI, TAC, RSRP, RSRQ, band, EARFCN, frequency)
 
Disclaimer:
- This is an advanced app with some features specific to only some devices/carriers. Many thanks to the community for the support and the questions and feedback.
- "Best signal available" doesn't mean this app can make LTE appear where there is none. There is no such thing as an "LTE signal booster" app, what actually happens is forcing the device to search again for the best signal rather than staying connected to an older, possible stale connection.
- Not all devices/carriers are supported for LTE bands and other advanced signal data. These are limitations of those devices.
- At this time, the in-app map is only used for showing current location. Though, Sprint+Pro users may have more feature. Support for more carriers is currently in active development.
- From user reports, Verizon sometimes provides tower location, but they are typically inaccurate.


If you work in the industry, feel free to email us if you have any knowledge that may be able to help us. (Our email can be found in-app)


By resetting the mobile connection, you can fix many common network related issues like: continuous low signal, data disconnection, poor call quality, signal instability, and more. You could also fix these issues by restart/reboot your phone, or you could just let LTE Discovery reset your radios in a few seconds. But, better signal quality is not a guarantee, especially if you are in a carrier zone with little coverage.


RATINGS: If you like this app, please support us by rating it 5 stars and helping with a +1, or let us know how we can improve the app. We are always working and striving to improve the app.

SUPPORT: If you notice any problems, please use the "Send debug email" option in-app and provide us as much information as possible about the problem and how we can recreate it.
